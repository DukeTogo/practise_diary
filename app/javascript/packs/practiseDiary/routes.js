import React from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

import Sessions from './components/sessions';

const App = (props) => (
  <Router>
    <div>
      <Route exact path='/' component={Sessions} />
    </div>
  </Router>
)

export default App;